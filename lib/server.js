'use strict';

const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class Server extends EventHandler {
  constructor(filters, serial, message, server, connection, options) {
    super();

    this.filters = filters;
    this.serial = serial.setProcessor(this.processRequest.bind(this));
    this.messageProvider = message;
    this.serverFactory = server;
    this.connectionProvider = connection;

    this.options = options.assign({
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      path: '*',
      port: 8000
    });

    this.server = null;
  }

  getOptions() {
    return this.options;
  }

  createMessage() {
    this.emit('debug', this, 'createMessage');
    return this.messageProvider.get();
  }

  bind(options) {
    this.emit('debug', this, 'bind', options);

    this.options.assign(options);

    const server = this.serverFactory.create();
    server.all(this.options.get('path'), this.handle.bind(this));

    this.server = server.listen(this.options.get('port'));
    return this;
  }

  release() {
    this.emit('debug', this, 'release');
    this.server.close();

    return this;
  }

  handle(request, response) {
    this.emit('debug', this, 'handle', request, response);

    this.serial
      .process(this.filters, [request])
      .then(this.handleRequest.bind(this, request, response))
      .catch(this.handleRequestError.bind(this, response));
  }

  processRequest(filter, [request]) {
    this.emit('debug', this, 'processRequest', request);
    return filter.get().connect(request);
  }

  handleRequest(request, response) {
    this.emit('debug', this, 'handleRequest', request, response);

    const connection = this.connectionProvider
      .get()
      .open(request, response);

    const headers = this.options.get('headers');

    Object.keys(headers).forEach((name) => {
      response.setHeader(name, headers[name]);
    });

    this.emit('connection', connection);
  }

  handleRequestError(response, error) {
    response.writeHead(error.status || 500);
    response.end(error.toString());

    this.emit('error', new Error('transport_request_not_handled', {
      origin: error
    }));
  }
}

module.exports = Server;
