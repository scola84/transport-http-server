'use strict';

const Abstract = require('@scola/transport-http-common');

class Connection extends Abstract.Connection {
  open(request, response) {
    this.emit('debug', this, 'open', request, response);

    this.request = request;
    this.response = response;

    this.bindListeners();
    return this;
  }

  close() {
    this.response.end();
    this.handleClose();

    return this;
  }

  canSend() {
    return this.response.finished === false;
  }

  bindListeners() {
    this.bindListener('data', this.request, this.handleReceiveData);
    this.bindListener('end', this.request, this.handleReceiveEnd);
    this.bindListener('close', this.response, this.handleClose);
  }

  unbindListeners() {
    this.unbindListener('data', this.request, this.handleReceiveData);
    this.unbindListener('end', this.request, this.handleReceiveEnd);
    this.unbindListener('close', this.response, this.handleClose);
  }

  handleClose() {
    this.unbindListeners();
    this.emit('close', this);
  }

  handleSend(message, close) {
    this.emit('debug', this, 'handleSend', message, close);

    const status = message.getStatus();
    const body = message.getBody();
    const headers = Object.assign({
      'Content-Length': Buffer.byteLength(body)
    }, message.getHeaders());

    this.response.writeHead(status, headers);
    this.response.write(body);

    if (close !== false) {
      this.close();
    }
  }

  handleSendError(message, error) {
    this.response.writeHead(error.status || 500);
    this.response.end(error.toString());

    super.handleSendError(message, error);
  }

  handleReceiveError(message, error) {
    this.response.writeHead(error.status || 500);
    this.response.end(error.toString());

    super.handleReceiveError(message, error);
  }
}

module.exports = Connection;
