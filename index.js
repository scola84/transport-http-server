'use strict';

const express = require('express');

const Async = require('@scola/async');
const Deep = require('@scola/deep');
const DI = require('@scola/di');
const Message = require('@scola/transport-message');

const Server = require('./lib/server');
const Connection = require('./lib/connection');

class Module extends DI.Module {
  configure() {
    this.inject(Server).with(
      this.array([]),
      this.instance(Async.Serial),
      this.provider(Message),
      this.factory(express),
      this.provider(Connection),
      this.instance(Deep.Map)
    );

    this.inject(Connection).with(
      this.array([]),
      this.provider(Async.Serial),
      this.provider(Message)
    );
  }
}

module.exports = {
  Connection,
  Module,
  Server
};
